export interface Property {
  title: string;
  value: string | boolean;
}

export interface Specification {
  id: string;
  name: string;
  properties: Property[];
}

export interface PropertyCreate {
  title: string;
  type: 'select' | 'string' | 'boolean';
  options?: string[],
  value?: string | boolean
}
