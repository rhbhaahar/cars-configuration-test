import type { Specification, PropertyCreate } from '@/interfaces/specification'
import { v4 as uuidv4 } from 'uuid'

export const specificationsData: Specification[] = [
  {
    id: uuidv4(),
    name: 'Sport',
    properties: [
      { title: 'Engine', value: 'V6 3.5L' },
      { title: 'Interior materials', value: 'Leather' },
      { title: 'Color', value: 'White' },
      { title: 'Wheel Rims', value: '20 inches' },
      { title: 'Disks model', value: 'BBS' },
      { title: 'Air suspension', value: true },
      { title: 'Signature on hood', value: 'Fast and furious' },
    ]
  },
  {
    id: uuidv4(),
    name: 'Prestige',
    properties: [
      { title: 'Engine', value: 'V6 3.5L' },
      { title: 'Interior materials', value: 'Leather' },
      { title: 'Color', value: 'Red' },
      { title: 'Wheel Rims', value: '19 inches' },
      { title: 'Disks model', value: 'BBQ' },
      { title: 'Air suspension', value: true },
      { title: 'Signature on hood', value: 'All inclusive' },
    ]
  },
  {
    id: uuidv4(),
    name: 'Standart',
    properties: [
      { title: 'Engine', value: 'F23 1.6L' },
      { title: 'Interior materials', value: 'Plastic' },
      { title: 'Color', value: 'Blue' },
      { title: 'Wheel Rims', value: '16 inches' },
      { title: 'Disks model', value: 'Standart' },
      { title: 'Air suspension', value: false },
      { title: 'Signature on hood', value: 'Loaded' },
    ]
  },
  {
    id: uuidv4(),
    name: 'Comfort',
    properties: [
      { title: 'Engine', value: 'V4 2L' },
      { title: 'Interior materials', value: 'Cloth' },
      { title: 'Color', value: 'Black' },
      { title: 'Wheel Rims', value: '19 inches' },
      { title: 'Disks model', value: 'Star' },
      { title: 'Air suspension', value: false },
      { title: 'Signature on hood', value: 'Fast and glorious' },
    ]
  }
]

export const propertiesList: PropertyCreate[] = [
  {
    title: 'Engine',
    options: [
      'V6 3.5L', 'V4 2L', 'F23 1.6L', 'Electric', 'Hybrid'
    ],
    type: 'select',
    value: ''
  },
  {
    title: 'Interior materials',
    options: [
      'Leather', 'Plastic', 'Cloth', 'Custom'
    ],
    type: 'select',
    value: ''
  },
  {
    title: 'Color',
    options: [
      'Red', 'Yellow', 'Black', 'White', 'Blue', 'Custom'
    ],
    type: 'select',
    value: ''
  },
  {
    title: 'Wheel rims',
    options: [
      '16 inches', '17 inches', '18 inches', '19 inches', '20 inches'
    ],
    type: 'select',
    value: ''
  },
  {
    title: 'Type of wheels',
    options: [
      'BBS', 'BBQ', 'Standart', 'Star', 'Custom', 'Reverse'
    ],
    type: 'select',
    value: ''
  },
  {
    title: 'Air suspension',
    type: 'boolean',
    value: false
  },
  {
    title: 'Signature on hood',
    type: 'string',
    value: ''
  },
]
