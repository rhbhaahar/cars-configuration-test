import { ref } from 'vue'
import { defineStore } from 'pinia'
import { specificationsData, propertiesList } from '@/prebuiltData'
import type { Specification, PropertyCreate } from '@/interfaces/specification'

export const useSpecificationStore = defineStore('specification', () => {
  const specifications = ref<Array<Specification>>(specificationsData);
  const properties = ref<Array<PropertyCreate>>(propertiesList);

  function addNewSpecification(specification): void {
    const index = specifications.value.findIndex(item => {
      return item.name === specification.name
    })

    if (index > -1) {
      specifications.value[index] = specification
    } else {
      specifications.value.push(specification)
    }
  }

  function purgeProperties(): void {
    properties.value.forEach(property => {
      if (typeof property.value === 'boolean') {
        property.value = false
      }

      if (typeof property.value === 'string') {
        property.value = ''
      }
    })
  }

  function addConfiguration(data): void {
    const configuration = {
      title: data.title,
      type: data.type,
      value: ''
    }
    if (data.type === 'select') {
      configuration.options = data.options
    }
    properties.value.push(configuration)
  }

  return { specifications, properties, addNewSpecification, purgeProperties, addConfiguration }
})
